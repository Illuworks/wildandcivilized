<?php

namespace App\Exceptions;


class MarshallException extends \Exception
{

    /**
     * Throw a new exception.
     *
     * @param string               $command
     * @param \ReflectionParameter $parameter
     *
     * @throws static
     *
     * @return void
     */
    public static function whileMapping($command, \ReflectionParameter $parameter)
    {
        throw new static("Unable to map parameter [{$parameter->name}] to command [{$command}]");
    }

}