<?php
/**
 * Created by PhpStorm.
 * User: teunm
 * Date: 22-11-2018
 * Time: 17:43
 */

namespace App\Api;


interface Dispatchable
{

    public function handle();

}
