<?php
/**
 * Created by PhpStorm.
 * User: teunm
 * Date: 22-11-2018
 * Time: 17:44
 */

namespace App\Api;


use App\Entities\ContactRequest;

class CreateContactRequest extends Api
{

    protected $data;

    /**
     * CreateContactRequest constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        $contactRequest = ContactRequest::make($this->data['email']);

        return $contactRequest;
    }

}
