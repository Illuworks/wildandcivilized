<?php

namespace App\Events;

use App\Entities\ContactRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ContactRequestCreated
{
    use Dispatchable, SerializesModels;

    public $contactRequest;

    /**
     * ContactRequestCreated constructor.
     *
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }
}
