<?php

namespace App\Http\Controllers\Traits;


use App\Api\Dispatchable;
use App\Exceptions\MarshallException;

trait Dispatches
{

    /**
     * Dispatch a dispatchable to its appropriate handler.
     *
     * @param  mixed $dispatchable
     * @return mixed
     */
    protected function dispatchService(Dispatchable $dispatchable)
    {
        return app('App\Dispatcher')->dispatch($dispatchable);
    }

    /**
     * Marshal a service and dispatch it to its appropriate handler.
     *
     * @param mixed $dispatchable
     * @param \ArrayAccess $source
     * @param array $extras
     *
     * @return mixed
     */
    protected function dispatchServiceFrom($dispatchable, \ArrayAccess $source, array $extras = [])
    {
        return $this->dispatch($this->marshal($dispatchable, $source, $extras));
    }

    /**
     * Marshal a dispatchable from the given array accessible object.
     *
     * @param string $dispatchable
     * @param \ArrayAccess $source
     * @param array $extras
     *
     * @return mixed
     */
    private function marshal($dispatchable, \ArrayAccess $source, array $extras = [])
    {
        $injected = [];
        $reflection = new \ReflectionClass($dispatchable);

        if ($constructor = $reflection->getConstructor()) {
            $injected = array_map(function ($parameter) use ($dispatchable, $source, $extras) {
                return $this->getParameterValue($dispatchable, $source, $parameter, $extras);
            }, $constructor->getParameters());
        }

        return $reflection->newInstanceArgs($injected);
    }

    /**
     * Get a parameter value for a marshaled command.
     *
     * @param string $dispatchable
     * @param \ArrayAccess $source
     * @param \ReflectionParameter $parameter
     * @param array $extras
     *
     * @return mixed
     */
    protected function getParameterValue($dispatchable, \ArrayAccess $source, \ReflectionParameter $parameter, array $extras = [])
    {
        if (array_key_exists($parameter->name, $extras)) {
            return $extras[$parameter->name];
        }
        if (isset($source[$parameter->name])) {
            return $source[$parameter->name];
        }
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }
        MarshallException::whileMapping($dispatchable, $parameter);
    }


}
