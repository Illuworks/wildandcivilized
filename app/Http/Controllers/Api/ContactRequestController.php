<?php
/**
 * Created by PhpStorm.
 * User: teunm
 * Date: 22-11-2018
 * Time: 17:38
 */

namespace App\Http\Controllers\Api;


use App\Api\CreateContactRequest;
use Illuminate\Http\Request;

class ContactRequestController extends Controller
{

    public function create(Request $request)
    {
        $this->dispatchServiceFrom(CreateContactRequest::class, $request);

        return $this->successResponse();
    }

}
