<?php

namespace App\Http\Controllers\Api;

class Controller extends \App\Http\Controllers\Controller
{

    /**
     * @param mixed $data [optional]
     * @param string $message [optional]
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($data = null, $message = null)
    {
        $response = ['success' => ($message ?: true), 'data' => $data];

        return response()->json($response);
    }

    /**
     * @param string $message [optional]
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($message = null)
    {
        $response = ['success' => false, 'error' => ($message ?: true)];

        return response()->json($response);
    }

    /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorsResponse($errors)
    {
        $response = ['success' => false, 'error' => true, 'errors' => $errors];

        return response()->json($response);
    }

}
