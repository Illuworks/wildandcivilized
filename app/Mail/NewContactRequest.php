<?php

namespace App\Mail;

use App\Entities\ContactRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewContactRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $contactRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Nieuwe contact aanvraag')
            ->view('emails.contact-request.new', ['contactRequest' => $this->contactRequest]);
    }
}
