<?php

namespace App\Mail;

use App\Entities\ContactRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactWithRequester extends Mailable
{
    use Queueable, SerializesModels;

    protected $contactRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contact')
            ->view('emails.contact-request.contact', ['contactRequest' => $this->contactRequest]);
    }
}
