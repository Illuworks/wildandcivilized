<?php

namespace App\Listeners;

use App\Mail\NewContactRequest;
use Illuminate\Support\Facades\Mail;

class NotifyAboutContactRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Mail::to(config('mail.to.address'))
            ->send(new NewContactRequest($event->contactRequest));
    }
}
