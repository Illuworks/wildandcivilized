<?php

namespace App\Listeners;

use App\Mail\ContactWithRequester;

class SendMailToContactRequester
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Mail::to($event->contactRequest->email)
            ->send(new ContactWithRequester($event->contactRequest));
    }
}
