<?php

namespace App\Providers;

use App\Bus\Decorators\UseDatabaseTransactions;
use Illuminate\Bus\Dispatcher;
use Illuminate\Support\ServiceProvider;

class BusServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $dispatcher = $this->app->make('App\Dispatcher');

        $dispatcher->pipeThrough([UseDatabaseTransactions::class]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Dispatcher', function ($app) {
            return new Dispatcher($app);
        });

        $this->app->alias(
            'App\Dispatcher', Dispatcher::class
        );
    }

}
