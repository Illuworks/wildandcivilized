<?php
/**
 * Created by PhpStorm.
 * User: teunm
 * Date: 22-11-2018
 * Time: 17:53
 */

namespace App\Entities;


class Model extends \Illuminate\Database\Eloquent\Model
{

    public function getRawAttribute($attribute)
    {
        return $this->attributes[$attribute];
    }

}
