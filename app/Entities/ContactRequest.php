<?php
/**
 * Created by PhpStorm.
 * User: teunm
 * Date: 22-11-2018
 * Time: 17:54
 */

namespace App\Entities;


use App\Events\ContactRequestCreated;

class ContactRequest extends Model
{

    protected $table = 'contact_requests';

    public static function make($email)
    {
        $inst = new static;
        $inst->email = $email;

        $inst->save();

        event(new ContactRequestCreated($inst));

        return $inst;
    }

}
