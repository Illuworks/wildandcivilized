import './bootstrap';
import './bootstrap-vue';
import StepsVue from './components/Steps';
import StepVue from './components/Step';
import ContactVue from './components/Contact';

Vue.component(StepsVue.name, StepsVue);
Vue.component(StepVue.name, StepVue);

const app = new Vue({
    el: '#app',

    components: {
        'contact': ContactVue
    },

    data: {
        lineDuration: 1200,
        ready: false,
    },

    mounted() {
        this.ready = true;
    }
});
