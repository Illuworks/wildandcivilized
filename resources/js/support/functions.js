/*
 * String
 */

String.prototype.ucFirst = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/*
 * Function
 */

export function defined(v) {
    return typeof v !== 'undefined';
}

export function isCallable (callback) {
    return (callback && typeof callback === 'function');
}

export function isObject(obj) {
    return (obj === Object(obj) && Object.prototype.toString.call(obj) !== '[object Array]');
}

export function is(obj, type) {
    const clas = Object.prototype.toString.call(obj).slice(8, -1);

    return obj !== undefined && obj !== null && clas === type;
}

export function objectToFormData (obj, form, namespace) {
    const fd = form || new FormData();
    let formKey;

    for (let property in obj) {
        if (obj.hasOwnProperty(property)) {
            if (namespace)
                formKey = namespace + '[' + property + ']';
            else
                formKey = property;

            // if the property is an object, but not a File use recursion
            if (obj[property] !== null && typeof obj[property] === 'object' && !(obj[property] instanceof File))
                objectToFormData(obj[property], fd, formKey);
            else {
                    fd.append(formKey, obj[property]);
            }
        }
    }

    return fd;
}

/*
 * Behavior
 */

export function scrollToTop () {
    $('body, html').animate({ scrollTop: 0 }, 400);
}