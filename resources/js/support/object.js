"use strict";

export default {

    build (arrayable, callback) {
        const built = {};

        for (let i in arrayable) {
            if (arrayable.hasOwnProperty(i)) {
                let builtItem = callback(arrayable[i], i);
                let key = Object.keys(builtItem)[0];

                built[key] = builtItem[key];
            }
        }

        return built;
    }

};