import router from './router';
import Vue from 'vue';
import loader from './modules/loader';

/**
 * Setting global router api for Laravel routes
 */

window.router = router;

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = Vue;

window.Vue.prototype.$http = deliverer;
window.Vue.prototype.$bus = bus;
window.Vue.prototype.$url = router;
window.Vue.prototype.$api = Api;
window.Vue.prototype.$laravel = laravel || {};
window.Vue.prototype.$loader = new Vue(loader);

/**
 * Load global Vue components
 */

