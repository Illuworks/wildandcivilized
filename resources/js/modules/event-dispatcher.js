class EventDispatcher {

    constructor() {
        this.subscriptions = {};
    }

    on(name, callback) {
        if (typeof this.subscriptions[name] === 'undefined')
            this.subscriptions[name] = [];

        this.subscriptions[name].push(callback);
    }

    emit(name, value) {
        const callbacks = this.subscriptions[name];
        let i;

        for (i in callbacks) {
            if (callbacks.hasOwnProperty(i))
                callbacks[i](value);
        }
    }

    promise(name, value) {
        const subscriptions = this.subscriptions[name];
        let promises = [];

        if (subscriptions) {
            promises = subscriptions.map(callback => {
                return new Promise((resolve, reject) => {
                    if (callback(value) === false)
                        reject(value);
                    else {
                        resolve(value);
                    }
                });
            });
        }

        return new Promise((resolve, reject) => {
            Promise.all(promises)
                .then(() => {
                    resolve(value);
                })
                .catch(() => {
                    reject(value);
                });
        });
    }

}

const dispatcher = new EventDispatcher();

export default dispatcher;
