export default {
    data: {
        startsCount: 0,
        stopsCount: 0,
    },

    created() {
        this.$bus.on('loading', bool => {
            this[bool ? 'start' : 'stop']();
        });
    },

    methods: {
        start() {
            this.startsCount++;
        },

        stop() {
            this.stopsCount++;
        },

        isLoading() {
            return this.stopsCount !== this.startsCount;
        }
    }
};
