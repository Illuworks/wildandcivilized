class Form {

    constructor(fields) {
        this.uid = Form.count++;
        this.$errors = {};
        this.$serialized = {};

        this.setFields(fields);
    }

    /**
     * Set data for initial field
     * @param fields
     * @returns {Form}
     */
    setFields(fields) {
        this.$fields = Object.keys(fields);
        this.$original = fields;

        this.setData(fields);

        return this;
    }

    /**
     * Add data for new field
     * @param field
     * @param value
     * @returns {Form}
     */
    addField(field, value) {
        this.$fields.push(field);
        this.$original[field] = value;

        this.setData({[field]: value});

        return this;
    }

    getFields() {
        return this.$fields;
    }

    /**
     * Set data only for yet existing and given fillable fields
     * @param data
     * @param fillable
     * @returns {*}
     */
    fillData(data, fillable) {
        return this.setData(data.filter((v, k) => fillable.indexOf(k) > -1));
    }

    setData(data) {
        _.each(this.$fields, field => {
            if (typeof this[field] !== 'function') {
                this[field] = data[field];
            }
        });

        return this;
    }

    /**
     * Reset form to original values
     * @returns {Form}
     */
    reset() {
        this.setFields(this.$original);

        return this;
    }

    /**
     * Clear all values
     * @returns {Form}
     */
    clear() {
        _.each(this.$fields, field => {
            this[field] = null;
        });

        return this;
    }

    /**
     * Set functions that will be used to validate the value of corresponding field
     * @param validators
     */
    setValidators(validators) {
        this.$validators = validators;

        return this;
    }

    /**
     * Set functions that will be used to serialize the value of corresponding field
     * @param serializers
     * @returns {Form}
     */
    setSerializers(serializers) {
        this.$serializers = {};

        _.each(serializers, (serializeValue, field) => {
            let serializer = (typeof serializeValue === 'string' ? (v) => (v ? v[serializeValue] : undefined) : serializeValue);
            this.$serializers[field] = serializer;
        });

        return this;
    }

    /**
     * Serializes and validates if any validators are given
     * @returns {Promise<any>}
     */
    serialize() {
        _.each(this.$fields, field => {
            this.serializeField(field);
        });

        return new Promise((resolve, reject) => {
            this.validate()
                .then(() => {
                    resolve(this.$serialized);
                })
                .catch(errors => {
                    reject(errors);
                });
        });
    }

    /**
     * Validates serialized values by given validators
     * @returns {Promise<any>}
     */
    validate() {
        return new Promise((resolve, reject) => {
            if (this.$validators != undefined) {
                _.each(this.$validators, (validator, field) => {
                    let result = validator(this.getValue(field));
                    if (result === false || result.success == false) {
                        this.$errors[field] = result.message || true;
                    }
                });

                if (this.hasErrors()) {
                    reject(this.$errors);
                }
            }

            resolve();
        });
    }

    hasErrors() {
        return Object.keys(this.$errors).length > 0;
    }

    /**
     * Get serialized value of given field
     * @param field
     * @returns {*}
     */
    getValue(field) {
        if (this.$serialized[field] == undefined)
            this.serializeField(field);

        return this.$serialized[field];
    }

    serializeField(field) {
        const value = (typeof this[field] === 'function' ? this[field].call(this) : this[field]);

        this.$serialized[field] = this.serializeValue(field, value);

        return this;
    }

    serializeValue(field, value) {
        if (typeof this.$serializers === 'undefined') return value;

        const serializer = this.$serializers[field];
        if (typeof serializer === 'undefined') return value;

        return serializer(value);
    }

}

export default Form;
