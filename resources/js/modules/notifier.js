import $ from 'jquery';

class Notifier {

    constructor() {
        this.init();
    }

    init() {
        this.container = $('<div style="position: absolute; z-index: 10090; right: 5%; top: 5%;"></div>');

        $('body').append(this.container);
    }

    error(message, title) {
        // TODO: This is just temporary content. Inside this method you would preferable call a package or plugin.

        this.publish('error', message, title);
    }

    errors(messages, title) {
        // TODO: This is just temporary content. Inside this method you would preferable call a package or plugin.
        const message = messages.join('<br>');

        this.publish('error', message, title);
    }

    success(message, title) {
        // TODO: This is just temporary content. Inside this method you would preferable call a package or plugin.

        this.publish('success', message, title);
    }

    publish(status, message, title) {
        // TODO: This is just temporary content. Inside this method you would preferable call a package or plugin.

        var title = '<h3>' + (title || Notifier.DEFAULTS[status].title) + '</h3>';
        var message = '<p>' + message + '</p>';

        var item = $('<div style="padding: 20px; margin-bottom: 20px; background:' + Notifier.DEFAULTS[status].bgColor + '; color: #fff;">' + title + message + '</div>');

        this.container.prepend(item);

        setTimeout(function () {
            item.remove();
            item = null;
        }, Notifier.DEFAULTS[status].displayTime);
    }

}

Notifier.DEFAULTS = {
    success: {
        title: 'Succes!',
        bgColor: 'green',
        displayTime: 3000,
    },
    error: {
        title: 'Oeps!',
        bgColor: 'red',
        displayTime: 5000,
    },
};

const notifier = new Notifier();

export default notifier;