class Memory {

    constructor() {
        this.storage = {};
    }

    /**
     * Get from storage. It won't return a value directly, but will return a Promise
     * @param key String; Id of data you want to store. I.e. 'post.index'
     * @param callback Callback will have a callback as a parameter. Use this on-success-callback when the data has been loaded/resolved.
     * @returns {Promise<any>}
     */
    remember(key, callback) {
        this.set(key, callback);

        return this.get(key);
    }

    /**
     * Store in memory
     * @param key String; Id of data you want to store. I.e. 'post.index'
     * @param value Mixed. Callback will have a callback as a parameter. Use this on-success-callback when the data has been loaded/resolved.
     */
    set(key, value) {
        if (this.storage[key] === undefined) {
            this.storage[key] = null;

            if (typeof value !== 'function') {
                this._store(key, value);
            }
            else {
                // Resolve callback
                value(response => {
                    this._store(key, response);
                });
            }
        }
    }

    /**
     * Store data in memory and emit event
     * @param key
     * @param value
     */
    _store(key, value) {
        this.storage[key] = value;
        bus.emit(`_memory.${key}`, value);
    }

    /**
     * Get from storage. It won't return a value directly, but will return a Promise
     * @param key String; Id of data you want to store. I.e. 'post.index'
     * @returns {Promise<any>}
     */
    get(key) {
        return new Promise((resolve, reject) => {
            if (this.storage[key] === undefined)
                reject();

            // When a request comes in before the callback has been resolved
            if (this.storage[key] === null) {
                bus.on(`_memory.${key}`, response => {
                    resolve(response);
                });
            }
            // When a request comes in after the callback has been resolved
            else {
                resolve(this.storage[key]);
            }
        });
    }

}

const memory = new Memory();

export default memory;
