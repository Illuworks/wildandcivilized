import $ from 'jquery';

class Deliverer {

    constructor(config) {
        this.cfg = $.extend({
            hasFiles: false,
            emitLoading: true,
            showNotifier: true,
            responseType: 'json',
        }, config);

        this.method = '';
        this.params = {};
    }

    set(config) {
        return new Deliverer(config);
    }

    parseData(data) {
        data = data || {};

        if (this.cfg.hasFiles) {
            data = objectToFormData(data);
        }

        return data;
    }

    get(url, params) {
        this.method = 'get';
        params = params ? '?' + $.param(params) : '';

        return this.procedure(axios.get(url + params, this.getHttpConfig()));
    }

    post(url, data) {
        this.method = 'post';

        return this.procedure(axios.post(url, this.parseData(data), this.getHttpConfig()));
    }

    put(url, data) {
        this.method = 'put';
        return this.procedure(axios.put(url, this.parseData(data), this.getHttpConfig()));
    }

    delete(url, params) {
        this.method = 'delete';
        params = params ? '?' + $.param(params) : '';

        return this.procedure(axios.delete(url + params, this.getHttpConfig()));
    }

    procedure(promise) {
        this.beforeHttp();

        return new Promise((resolve, reject) => {
            promise
                .then(response => {
                    this.onSuccess(response, resolve)
                })
                .catch(error => {
                    this.onError(error, reject)
                });
        });
    }

    beforeHttp() {
        this.startLoading();
    }

    onSuccess(response, resolve) {
        this.stopLoading();

        if (this.cfg.showNotifier) {
            if (response.data.error)
                notifier.error(response.data.error);
            if (response.data.success && typeof response.data.success === 'string')
                notifier.success(response.data.success);
        }

        resolve(response.data);
    }

    onError(error, reject) {
        this.stopLoading();

        if (this.cfg.showNotifier) {
            // TODO: catch correct message
            // if (error.response && error.response.data)
            //     notifier.error(error.response.data);
        }

        reject(error);
    }

    getHttpConfig() {
        const cfg =  {
            responseType: this.cfg.responseType,
        };

        if (this.needsJson()) {
            cfg.headers = {
                'Content-Type': 'application/vnd.api+json'
            };
        }

        return cfg;
    }

    hasFormData() {
        return (typeof this.data === 'object' && is(this.data, 'FormData'));
    }

    needsJson() {
        return (['post', 'put', 'delete'].indexOf(this.method) > -1);
    }

    startLoading() {
        if (this.cfg.emitLoading)
            bus.emit('loading', true)
    }

    stopLoading() {
        if (this.cfg.emitLoading)
            bus.emit('loading', false)
    }

}

const deliverer = new Deliverer();

export default deliverer;
