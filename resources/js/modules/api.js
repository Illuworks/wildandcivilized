class Api
{

    static getUrl(endpoint, endpointParams, apiVersion) {
        apiVersion = apiVersion || 'api';
        const params = Object.assign({}, endpointParams || {});

        return router.route(`${apiVersion}.${endpoint}`, params);
    }

    static get(endpoint, endpointParams, params, apiVersion) {
        const url = this.getUrl(endpoint, endpointParams, apiVersion);
        params = params || {};

        return deliverer.get(url, params);
    }

    static remember(endpoint, endpointParams, params, apiVersion) {
        const url = this.getUrl(endpoint, endpointParams, apiVersion);

        return memory.remember(`_api.${url}`, onSuccess => {
            this.get(endpoint, endpointParams, params, apiVersion)
                .then(response => {
                    onSuccess(response);
                });
        });
    }

    static post(endpoint, endpointParams, data, apiVersion) {
        data = data || {};

        return deliverer.post(this.getUrl(endpoint, endpointParams, apiVersion), data);
    }

    static put(endpoint, endpointParams, data, apiVersion) {
        data = data || {};

        return deliverer.put(this.getUrl(endpoint, endpointParams, apiVersion), data);
    }

    static delete(endpoint, endpointParams, params, apiVersion) {
        params = params || {};

        return deliverer.delete(this.getUrl(endpoint, endpointParams, apiVersion), params);
    }

}

export default Api;
