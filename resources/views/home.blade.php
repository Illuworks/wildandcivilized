@extends('layouts.master')

@prepend('styles')
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endprepend()

@section('app')
    <main id="site-main">
        <div id="site-content">
            <steps v-if="ready">
                <step key="1" :duration="lineDuration">We are web athletes</step>
                <step key="2" :duration="lineDuration">We build one-of-a-kind websites</step>
                <step key="3" :duration="lineDuration">And complex web applications</step>
                <step key="4" :duration="lineDuration">We are perfectionists</step>
                <step key="5" :duration="lineDuration">We are shape-shifters</step>
                <step key="6" :duration="lineDuration">We are both artist and programmer</step>
                <step key="7" :duration="lineDuration">But most of all</step>
                <step key="8" :duration="lineDuration">We are</step>
                <step key="9" :duration="500" class="is-bold">The wild</step>
                <step key="10" :duration="500" class="is-bold">And</step>
                <step key="11" :duration="lineDuration + 500" class="is-bold should-leave-slow">Civilized</step>
                <step key="12" :duration="false">
                    <contact></contact>
                </step>
            </steps>
        </div>
    </main>
@endsection()

@prepend('scripts')
    <script src="{{ asset('js/home.js') }}"></script>
@endprepend()
