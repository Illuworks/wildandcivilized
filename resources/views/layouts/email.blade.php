<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body style="background: #f6f6f6">
<style>
    @media only screen and (max-width: 600px) {
        .inner-body {
            width: 100% !important;
        }

        .footer {
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }
</style>

<table class="full" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td valign="middle" align="center" style="background: #f6f6f6; padding: 15px;">
            <table class="wrapper" width="594" cellpadding="0" cellspacing="0" style="font-family: sans-serif">
                <tr>
                    <td align="center" style="padding: 15px; background: #ffffff;">
                        <table class="content" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="header">

                                </td>
                            </tr>

                            <tr>
                                <td class="main" style="padding: 30px 15px;">
                                    @yield('content')
                                </td>
                            </tr>

                            <tr>
                                <td style="border-top: 2px solid #eeeeee; padding: 30px 15px;" align="center">

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
