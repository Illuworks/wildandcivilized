<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    @stack('styles')
</head>

<body id="@yield('id')" class="@stack('class')">

<div id="app">
    @yield('app')

    @stack('root')
</div>

<!-- Scripts -->
<script>
    window.laravel = {
        errors: {!! json_encode($errors->toArray()) !!},
        old_values: {!! json_encode(old()) !!},
        csrf_token: '{{ csrf_token() }}',
        request: {
            url: '{{ request()->url() }}'
        },
        url: {
            asset: '{{ asset('') }}',
        }
    };
</script>
@stack('scripts')
</body>
</html>
