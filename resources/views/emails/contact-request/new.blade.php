@extends('layouts.email')

@section('content')
    <h3>Contactaanvraag via The Wild & Civilized</h3>

    <p>
        {{ $contactRequest->email }}
    </p>
@endsection()
